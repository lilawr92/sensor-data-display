import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { OptionsBarComponent } from './options-bar/options-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    OptionsBarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
