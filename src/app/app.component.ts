import { Component } from '@angular/core';
import jsonSensorData from '../assets/data/sensor_readings.json';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Sensor Data Display';
  sensorData: Array<Object>;
  medianData: Array<Object>;
  medianLoaded: boolean;
  viewState: string;

  ngOnInit(): void {
    this.sensorData = jsonSensorData;
    this.viewState = 'ALL';
    this.medianData = this._calculateMedians(jsonSensorData);
    this.filterData = this.filterData.bind(this);
    this.sortData = this.sortData.bind(this);
    this.setView = this.setView.bind(this);

    console.log("Sensor Data Application Loaded.");
  }

  sortData(key: string): void {
    this.sensorData = _.orderBy(this.sensorData, key, 'asc');
  }

  filterData(filters: any): void {
    this.sensorData = _.filter(jsonSensorData, filters);
  }

  setView(view: string): void {
    this.viewState = view;
  }

  _calculateMedians(data: Array<Object>): Array<Object> {
    if (!data)
      return [];

    let medians = {};
    let medianDataSet = [];
    data.forEach(reading => {
      if (!medians[reading['sensor_type']]) {
        medians[reading['sensor_type']] = true;
        const sensorTypeData = _.filter(data, ['sensor_type', reading['sensor_type']]);
        medianDataSet.push(this._calculateMedian(sensorTypeData));
      }
    });

    return medianDataSet;
  }

  _calculateMedian(data: Array<Object>): Object {
    const sortedData = _.orderBy(data, 'reading', 'asc');
    const median = sortedData[data.length / 2];
    if(data.length % 2 == 0) {
      const upperMedian = sortedData[data.length / 2];
      median['median'] = (median['reading'] + upperMedian['reading']) / 2;
    } else median['median'] = median['reading'];
    return median;
  }
}

