import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'options-bar',
  templateUrl: './options-bar.component.html',
  styleUrls: ['./options-bar.component.scss']
})

export class OptionsBarComponent implements OnInit {

  constructor() { }
  optionChoice: string = "SORT";
  filters: any = {};

  @Input() handleFilterData: Function;
  @Input() handleSortData: Function;
  @Input() handleSetView: Function;
  @Input() viewState: string;

  setView(view: string): void {
    this.handleSetView(view);
  }

  loadFilters(): void {
    this.handleFilterData(this.filters);
  }

  resetFilters(): void {
    this.filters = {};
    this.handleFilterData({});
  }

  onFilterChange(event: any): void {
    if (event.target.value.length === 0) {
      delete this.filters[event.target.id];
      return;
    }
    this.filters[event.target.id] = event.target.value;
  }

  ngOnInit(): void { }
}
