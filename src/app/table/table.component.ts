import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import dataFormats from '../../assets/constants/data_view_formats.json';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  dataFormat: any;
  pages: number;
  page: number = 1;
  pageSize: number = 50;
  displayData: Array<any>;

  @Input() data: Array<any>;
  @Input() dataKey: string;
  @Input() currentSort: string;

  changePage(pageNumber: number): void {
    this.page = pageNumber;
    const itemLimit = ((this.pageSize * this.page) > this.data.length) ? this.data.length -1 : (this.pageSize * this.page);
    this.displayData = this.data.slice(this.pageSize * (this.page-1), itemLimit);
  }
  
  ngOnChanges(changes: SimpleChanges) {
    this.pages = Math.ceil(this.data.length/this.pageSize);
    this.changePage(1);
  }

  ngOnInit(): void {
    this.dataFormat = dataFormats[this.dataKey];
    this.pages = Math.ceil(this.data.length/this.pageSize);
    this.displayData = this.data.slice(this.pageSize * (this.page-1), this.pageSize * (this.page));
  }
}
