# Sensor Data Display

This single page project loads environmental sensor data from a local json file and displays it in a tabular format. 

The header bar contains options to sort or filter or summarize the data. 

* You can sort your data by time/sensor type
* You can filter the data by sensor type and /or name
* You can view an aggregation of the data to view the median of each sensor type

# Developer set-up

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.1.

## Installing the project

* Clone to you local machine
* If you dont have Angular Cli installed installed run:  
  `npm install -g @angular/cli`
* From the root folder run `npm install`

## To run the project
Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`.



